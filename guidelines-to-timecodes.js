import { KdenliveFile } from "./lib/KdenliveFile.js";
import { toFfmetadata, toMarkdown } from "./lib/chapters.js";

const filename = process.argv[2];
const type = process.argv[3] || "txt";
console.error(`processing file ${filename}`);

const project = new KdenliveFile(filename);
const guidesContent = project.project.window.document.querySelector("property[name=\"kdenlive:sequenceproperties.guides\"]")?.innerHTML || "";
// TODO this will likely contain xml escaped entities, these might need conversion here.
const guidesJson = JSON.parse(guidesContent);

// collecting the chapters in the chapter format
const chapters =  [];
for (const guide of guidesJson) {
	const seconds = project.frameToSeconds(guide.pos);
	chapters.push({in: seconds, name: guide.comment});
}

if (type === "txt") {
	toMarkdown(chapters);
} else if (type === "ffmetadata") {
	toFfmetadata(chapters, project.frameRate);
} else {
	throw(`Error: output format ${type} not found!`);
}
