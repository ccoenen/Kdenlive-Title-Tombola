import { KdenliveFile } from "./lib/KdenliveFile.js";
import { KdenliveTitle } from "./lib/KdenliveTitle.js";
import { importSpreadsheet } from "./lib/spreadsheetReader.js";
import { options } from "./lib/argumentParser.js";
import { encodeXML } from "entities";

const project = new KdenliveFile(options.kdenliveproject);
const desiredTitles = importSpreadsheet(options.spreadsheet);
project.findTemplateClip();

let counter = 0;
for (const titleData of desiredTitles) {
	counter++;
	console.log(`processing clone ${counter}: ${titleData.clipname}`);
	const id = `producer-template-tombola-${counter}`;

	const cloned = project.getTemplateClone();
	cloned.setAttribute("id", id);
	cloned.querySelector("property[name='kdenlive:clipname']").innerHTML = encodeXML(titleData.clipname || `cloned number ${counter}`);
	const titleNode = cloned.querySelector("property[name='xmldata']");
	const title = new KdenliveTitle(titleNode.innerHTML);
	title.replaceText(titleData);
	titleNode.innerHTML = title.encode();

	project.addProducer(cloned);
}

console.log(`writing to ${options.output}`);
project.write(options.output);
