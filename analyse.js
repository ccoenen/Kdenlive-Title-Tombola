import chalk from "chalk";
import { KdenliveFile } from "./lib/KdenliveFile.js";

console.log(process.argv[2]);
const project = new KdenliveFile(process.argv[2]);

console.log(chalk.blue("# Producers"));
for (const producer of Object.values(project.producers)) {
	const resource = producer.querySelector("property[name=\"resource\"]");
	console.log(`- ${chalk.yellow(producer.getAttribute("id"))}: ${resource.innerHTML}`);
}

// main_bin has a ton of docproperties, that contain for example the target render path!

console.log(chalk.blue("\n# Playlists"));
for (const playlist of Object.values(project.playlists)) {
	const entries = playlist.querySelectorAll("entry,blank") || [];
	const mappedEntries = [];
	for (const entry of entries) {
		mappedEntries.push(entry.getAttribute("producer") || chalk.gray("blank"));
	}
	console.log(`- ${chalk.yellow(playlist.getAttribute("id"))}:\n  ${mappedEntries.join(", ")}`);
}

console.log(chalk.blue("\n# Tractors"));
for (const tractor of Object.values(project.tractors)) {
	const trackName = tractor.querySelector("property[name=\"kdenlive:track_name\"]")?.innerHTML || "";
	const tracks = tractor.getElementsByTagName("track");
	const tracksMapped = [];
	for (const track of tracks) {
		tracksMapped.push(track.getAttribute("producer") || chalk.red("unknown"));
	}
	console.log(`- ${chalk.yellow(tractor.getAttribute("id"))} "${trackName}": ${tracksMapped.join(", ")}`);
}
