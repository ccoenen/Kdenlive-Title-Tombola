import { JSDOM } from "jsdom";
import { readFileSync, writeFileSync } from "fs";

export class KdenliveFile {
	constructor(filename) {
		this.project;
		this.templateClip;
		this.playlists = {};
		this.producers = {};
		this.tractors = {};
		this.frameRate = -1;
		this.mainBin;
		this.templatePlaylistEntry;
		this.filename = filename;
		this.highestKdenliveId = -1;
		this.load();
	}

	load() {
		this.project = new JSDOM(readFileSync(this.filename), { contentType: "text/xml" });

		this.frameRate = parseInt(this.project.window.document.querySelector("profile[frame_rate_num]").getAttribute("frame_rate_num"), 10);

		for (const playlist of this.project.window.document.getElementsByTagName("playlist")) {
			this.playlists[playlist.getAttribute("id")] = playlist;
		}
		this.mainBin = this.playlists["main_bin"];

		for (const producer of this.project.window.document.getElementsByTagName("producer")) {
			this.producers[producer.getAttribute("id")] = producer;
		}

		for (const tractor of this.project.window.document.getElementsByTagName("tractor")) {
			this.tractors[tractor.getAttribute("id")] = tractor;
		}

		this.findHighestKdenliveId();
	}

	write(filename) {
		this.filename = filename;
		const xmlOutput = this.project.serialize();
		writeFileSync(filename, xmlOutput);
	}

	findHighestKdenliveId() {
		const elements = this.project.window.document.querySelectorAll("property[name='kdenlive:id']");
		for (const element of elements) {
			const kdenliveId = parseInt(element?.innerHTML, 10);
			if (!isNaN(kdenliveId)) {
				this.highestKdenliveId = Math.max(kdenliveId, this.highestKdenliveId);
			}
		}
	}

	findTemplateClip() {
		const producers = this.project.window.document.getElementsByTagName("producer");

		for (const producer of producers) {
			const id = producer.getAttribute("id");

			const clipname = producer.querySelector("property[name='kdenlive:clipname']");
			if (clipname && clipname.innerHTML === "template-title") {
				this.template = producer;
				this.templatePlaylistEntry = this.mainBin.querySelector(`entry[producer='${id}'`);
			}
		}

		if (!this.template) {
			throw `we did not find a clip named 'template-title' in ${this.filename}`;
		}

		this.template.removeChild(this.template.querySelector("property[name='kdenlive:file_hash']"));
	}

	getTemplateClone() {
		const node = this.template.cloneNode(true);
		node.querySelector("property[name='kdenlive:id']").innerHTML = ++this.highestKdenliveId;
		return node;
	}

	addProducer(producer) {
		// needs to be before playlist, apparently.
		this.mainBin.parentNode.insertBefore(this.project.window.document.createTextNode(" "), this.mainBin);
		this.mainBin.parentNode.insertBefore(producer, this.mainBin);
		this.mainBin.parentNode.insertBefore(this.project.window.document.createTextNode("\n"), this.mainBin);

		const clonedPlaylistEntry = this.templatePlaylistEntry.cloneNode();
		clonedPlaylistEntry.setAttribute("producer", producer.getAttribute("id"));
		this.mainBin.appendChild(this.project.window.document.createTextNode("  "));
		this.mainBin.appendChild(clonedPlaylistEntry);
		this.mainBin.appendChild(this.project.window.document.createTextNode("\n"));
	}

	frameToSeconds(frame) {
		return frame / this.frameRate;
	}
}

const TIMECODE_REGEX = /^(?<h>\d+):(?<m>\d+):(?<s>\d+).(?<ms>\d+)$/;
export function timecodeToSeconds(timecode) {
	const match = TIMECODE_REGEX.exec(timecode);
	if (!match) throw `timecode could not be parsed: ${timecode}`;
	const h = parseInt(match.groups.h, 10);
	const m = parseInt(match.groups.m, 10);
	const s = parseInt(match.groups.s, 10);
	const ms = parseInt(match.groups.ms, 10);
	return 3600 * h + 60 * m + s + ms / 1000;
}

export function secondsToTimecode(seconds) {
	let remainder = seconds;
	const h = Math.floor(remainder / 3600);
	remainder -= h * 3600;
	const m = Math.floor(remainder / 60);
	remainder -= m * 60;
	const s = Math.floor(remainder);
	remainder -= s;
	const ms = Math.round(remainder * 1000);

	return `${h < 10 ? "0"+h : h}:${m < 10 ? "0"+m : m}:${s < 10 ? "0"+s : s}.${ms < 10 ? "00"+ms : (ms < 100 ? "0"+ms : ms)}`;
}
