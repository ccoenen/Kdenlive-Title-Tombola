import { JSDOM } from "jsdom";
import { decodeXML, encodeXML } from "entities";

export class KdenliveTitle {
	constructor(xmldata) {
		const decoded = decodeXML(xmldata);
		this.data = new JSDOM(decoded, { contentType: "text/xml" });
	}

	replaceText(map) {
		for (const item of this.data.window.document.querySelectorAll("item[type='QGraphicsTextItem']")) {
			const content = item.querySelector("content");
			const text = content.innerHTML;
			if (text.startsWith("{{") && text.endsWith("}}")) {
				content.removeAttribute("box-width");
				content.removeAttribute("box-height");
				
				const tagName = text.substring(2, text.length - 2);
				content.innerHTML = encodeXML(map[tagName] || "");
			}

		}
	}

	encode() {
		return encodeXML(this.data.serialize());
	}
}
