import { program } from "commander";

program
	.name("Kdenlive Title Tombola")
	.description("Creates a bunch of titles to be used in your Kdenlive project.")
	.requiredOption("-k --kdenliveproject <path>", "path to your .kdenlive file")
	.requiredOption("-s --spreadsheet <path>", "path to your spreadsheet - should work with .ods or .xlsx")
	.option("-o --output [path]", "path to the generated output .kdenlive file");

program.parse();

export const options = program.opts();

if (!options.output || options.output === options.kdenliveproject) {
	const bits = options.kdenliveproject.split(".");
	bits.splice(bits.length-1, 0, "output");
	options.output = bits.join(".");
}
