import * as XLSX from "xlsx/xlsx.mjs";
import fs from "fs";

XLSX.set_fs(fs);

export function importSpreadsheet(filename) {
	const doc = XLSX.readFile(filename);
	const workbook = doc.SheetNames[0];
	return XLSX.utils.sheet_to_json(doc.Sheets[workbook]);
}
