import chalk from "chalk";

import { secondsToTimecode } from "./KdenliveFile.js";


/*
Formats chapters. Chapters should look like this:
[
	{in: 0, out: 3601.25, name: "hello world"},
	{in: 3601.25, out: 3800.0, name: "bye"},
]

'in' is the start of a chapter, in seconds (Number).
'out' is the end of a chapter, in seconds (Number). This one is optional, but for formats where it is required (ffmetadata) it will be automatically populated.
'name' is the name of a chapter (String).
*/


export function toMarkdown(chapters) {
	for (const chapter of chapters) {
		console.log(`- ${chalk.blue(secondsToTimecode(chapter.in))}: ${chalk.yellow(chapter.name)} (${Math.floor(chapter.in)}s)`);
	}
}

export function toFfmetadata(chapters, frameRate) {
	console.log(";FFMETADATA1");

	let thisChapter;
	for (const nextChapter of chapters) {
		if (thisChapter && !thisChapter.out) {
			thisChapter.out = nextChapter.in;
		}
		thisChapter = nextChapter;
	}
	thisChapter.out = thisChapter.out || thisChapter.in; // we don't have an end, this way at least we have a valid chapter IN.

	for (const chapter of chapters) {
		const output = `[CHAPTER]
TIMEBASE=1/${frameRate}
START=${Math.floor(chapter.in * frameRate)}
END=${Math.floor(chapter.out * frameRate)}
title=${chapter.name}
`;

		console.log(output);
	}
}
