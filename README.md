# Kdenlive Title Tombola

Create multiple Title producers for your kdenlive project

![title clip interface from kdenlive](doc/title-clip.png)


## Usage

You need a kdenlive project with your desired template. That template must be named `template-title` and can contain any layout and any number of text items and animations. All text items starting with `{{` and ending with `}}` will be replaced by the script. Look at `example-files/template.kdenlive` for a tiny example.

You also need a spreadsheet with the data you would like to fill into the generated title cards. The first row must be the title, and it is matched against the text in your title card. A column called `line1` will be filled into a text container that has `{{line1}}` written into it. Look at `example-files/input.ods` for a tiny example.

If you have a column `clipname`, this will also be the title of your clip. If this is not provided, a name is automatically generated.

With these files in place, call the script with

`node index.js --kdenliveproject template.kdenlive --spreadsheet list.ods`

You can try this with the provided example files:

`node index.js --kdenliveproject example-files/template.kdenlive --spreadsheet example-files/input.ods`


## Requirements

Tested up to kdenlive 23.08.4, but I've been using this script since kdenlive 21.12.


## Other Tools included

### analyse.js

Will give you a very basic overview over the contents of a kdenlive-file:
`node analyse.js example-files/template.kdenlive`


### named-track-to-timecodes.js

Will list all "in" timecodes of a given named track:
`node named-track-to-timecodes.js example-files/template.kdenlive trackname`

By default, this will output a human-readable text format, but you can also specify ffmetadata output like this:
`node named-track-to-timecodes.js example-files/template.kdenlive trackname ffmetadata > chapters.txt`

These can be integrated into a video file with ffmpeg
`ffmpeg -i input-video.mp4 -i chapters.txt -map_metadata 1 -codec copy output-video-with-chapters.mp4`


### guidelines-to-timecodes.js

Will list all guidelines:
`node guidelines-to-timecodes.js example-files/template.kdenlive`

As for `named-track-to-timecodes.js`, you can also output to ffmetadata:
`node guidelines-to-timecodes.js example-files/template.kdenlive trackname ffmetadata > chapters.txt`

integration works same as above, of course.


## Notes

There are currently two unintended differences with the generated file:

- it does not have the XML preamble
- it escapes a few more characters than the input

Both do not seem to have any effect on kdenlive and upon saving the file in kdenlive, the two things are returning to the original behavior.

MLT XML format is explained here: https://www.mltframework.org/docs/mltxml/


## Further Reading

- [FFMETADATA format](https://ffmpeg.org/ffmpeg-formats.html#Metadata-2)
