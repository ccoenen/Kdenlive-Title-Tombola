import { KdenliveFile, timecodeToSeconds } from "./lib/KdenliveFile.js";
import { toFfmetadata, toMarkdown } from "./lib/chapters.js";

const filename = process.argv[2];
const trackname = process.argv[3];
const type = process.argv[4] || "txt";

console.error(`processing file ${filename}`);
console.error(`looking for track ${trackname}`);

const project = new KdenliveFile(filename);
let foundPlaylists = [];

for (const tractor of Object.values(project.tractors)) {
	const currentTrackName = tractor.querySelector("property[name=\"kdenlive:track_name\"]")?.innerHTML || "";
	if (currentTrackName === trackname) {
		for (const track of tractor.getElementsByTagName("track")) {
			foundPlaylists.push(track.getAttribute("producer"));
		}
	}
}
console.error(`found ${foundPlaylists.length} playlists: ${foundPlaylists.join(", ")}`);

const chapters = [];

for (const playlistId of foundPlaylists) {
	console.error(`${playlistId}`);
	const playlist = project.playlists[playlistId];
	let cursor = 0;
	for (const item of playlist.querySelectorAll("entry,blank")) {
		if (item.tagName === "blank") {
			cursor += timecodeToSeconds(item.getAttribute("length"));
		}
		if (item.tagName === "entry") {
			const producerId = item.getAttribute("producer");
			const producer = project.producers[producerId];
			const clipname = producer.querySelector("property[name=\"kdenlive:clipname\"]")?.innerHTML;

			const chapter = { in: cursor, name: clipname };
			cursor += timecodeToSeconds(item.getAttribute("out"));
			cursor -= timecodeToSeconds(item.getAttribute("in"));
			chapter.out = cursor;

			chapters.push(chapter);
		}
	}
}

if (type === "txt") {
	toMarkdown(chapters);
} else if (type === "ffmetadata") {
	toFfmetadata(chapters, project.frameRate);
} else {
	throw(`Error: output format ${type} not found!`);
}
